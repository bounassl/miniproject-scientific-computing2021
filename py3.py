# -*- coding: utf-8 -*-
"""
Created on Wed Jan  5 00:28:20 2022

@author: acer
"""
import numpy as np
import math


def SBD(elem, ci, beta_pitch):
    SBD = np.zeros(elem)
    SBD_pourcentage = 0
    for i in range(0, elem):
        SBD[i] = (3 / math.pi) * ci[i] * 0.05 * np.cos(beta_pitch[i] * math.pi / 180)
    SBD_pourcentage = np.sum(SBD) * 100
    return SBD_pourcentage
