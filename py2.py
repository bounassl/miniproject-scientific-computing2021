# -*- coding: utf-8 -*-
"""
Created on Mon Jan  3 00:42:31 2022

@author: acer
"""

import numpy as np
import matplotlib.pyplot as plt
import math


def fun3(lamdaR, elem, rdi, N, CD, CL, alpha):
    lamda = np.zeros(elem)
    ci = np.zeros(elem)
    nume = np.zeros(elem)
    denom = np.zeros(elem)
    beta = np.zeros(elem)
    beta_pitch = np.zeros(elem)
    for i in range(0, elem):
        lamda[i] = lamdaR * rdi[i]
        beta[i] = np.arctan(2 / (3 * lamda[i])) * 180 / math.pi
        beta_pitch[i] = beta[i] - alpha[i]
        nume[i] = 16 * math.pi * rdi[i]
        denom[i] = (
            9
            * np.sqrt(math.pow(lamda[i], 2) + 4 / 9)
            * (lamda[i] + ((2 * CD[i]) / (3 * CL[i])))
        )
        ci[i] = (1 / (CL[i] * N)) * (nume[i] / denom[i])
    return ci, lamda, beta, beta_pitch
