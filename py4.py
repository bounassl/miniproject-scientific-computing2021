# -*- coding: utf-8 -*-
"""
Created on Wed Jan  5 01:12:26 2022

@author: acer
"""
import numpy as np
import math


def CM(elem, lamda, ci_interp, CL_interp, CD_interp, beta, rdi, x):
    CMt = np.zeros(elem)
    CMb = 0
    for i in range(0, elem):
        CMt[i] = ((0.05 * 3) / math.pi) * (
            ((4 / 9) + lamda[i] ** 2)
            * ci_interp[i]
            * rdi[i]
            * (
                (CL_interp[i] * np.sin(beta[i] * math.pi / 180))
                - (CD_interp[i] * x * np.cos(beta[i] * math.pi / 180))
            )
        )
    CMb = np.sum(CMt)
    return CMb
