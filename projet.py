"""
@author: Liliane Bou Nassif
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import py1
from py1 import fun2
import py2
from py2 import fun3
import py3
from py3 import SBD
import py4
from py4 import CM

# pip install openpyxl

# Using Book1 file, plot curves of lift coefficient(CL), drage coefficient (CD)
# and lift-to-drag ratio (LDR) CL/CD as a function of the angle of attack alpha
# Make plots for 0°<alpha <55°.
df = pd.read_excel(r"C:\Users\acer\Desktop\project scientific\Book1.xlsx")

size = np.shape(df)[0]
r = 0
for i in range(size):
    if df["alpha"][i] == 55:
        r = i
plt.figure()
plt.plot(df["alpha"][0 : r + 1], df["CL"][0 : r + 1], label="CL versus alpha")
plt.plot(df["alpha"][0 : r + 1], df["CD"][0 : r + 1], label="CD versus alpha")
plt.xlabel("alpha")
plt.ylabel("CL,CD")
plt.legend()

df["LDR"] = df["CL"][0 : r + 1] / df["CD"][0 : r + 1]
plt.figure()
plt.plot(df["alpha"], df["LDR"], label="LDR versus alpha")
plt.xlabel("alpha")
plt.ylabel("lDR")
plt.legend()

# Determine the angle of best lift-to-drag ratio (LDR) which will be used for
# the design.
LDR_best = 0
alpha_best = 0
CL_best = 0
CD_best = 0
for i in range(size):
    LDR_best = np.max(df["LDR"])
    if df["LDR"][i] == np.max(df["LDR"]):
        alpha_best = df["alpha"][i]
        CL_best = df["CL"][i]
        CD_best = df["CD"][i]
print("best lift to drag ratio LDR=", LDR_best)
print("alpha at best LDR =", alpha_best)
print("CL at best LDR =", CL_best)
print("CD at best LDR =", CD_best)

# The main objective is to plot the blade shape from R to Rhub, The span of
# eachblade is divided into 19 blade elements along the radius
# The dimensionless radius rdi = ri/R for each element center is considered

elem = 19
rdi = np.zeros(elem)
rdi[0] = 0.075
for i in range(1, elem):
    rdi[i] = rdi[i - 1] + 0.05
print("rdi =", rdi)

# We consider a rotor equipped with 3 blades: N=3. For lamdaR from1 to 10
# create 10 table : rdi, lamda= rdi*lamdaR,betai (flow angle in °), alphai
# (flow incidence in °), betapi (pitch angle in °), Cli, Cdi, ci=Ci/R
# (dimensionless chord), 𝑖∈[1,19].Plot c(rd), lamdaR from 1 to 10.
N = 3  # number of blades
CL = np.zeros(elem)
CD = np.zeros(elem)
alpha = np.zeros(elem)
for i in range(0, elem):
    CL[i] = CL_best
    CD[i] = CD_best
    alpha[i] = alpha_best
plt.figure()
for lamdaR in range(1, 11):
    ci, lamda, beta, beta_pitch = fun3(lamdaR, elem, rdi, N, CD, CL, alpha)
    plt.plot(rdi, ci, label=lamdaR)
plt.xlabel("rdi")
plt.ylabel("ci")
plt.legend()
print(
    "The ci value at hub for 𝜆𝑟=1 is high in comparison to the other 𝜆𝑟 , so"
    " ci near the blade root has unrealistic value since it is greater than"
    " the blade radius."
)

# evaluate the lamdaR of american wind turbine(slow turbine)
Ns = 32  # number of blades
lamdaRs_new = 0
for lamdaRs in np.arange(1, 10, 0.3):
    cis, lamdas, betas, beta_pitchs = fun3(lamdaRs, elem, rdi, Ns, CD, CL, alpha)
    if "{:.2f}".format(cis[-1]) == "0.10":
        lamdaRs_new = lamdaRs
        print("𝜆𝑟 of american wind turbine =", lamdaRs_new)
# evaluate the lamdaR of Vestas V90-2.0 MW(fast turbine)
lamdaRf_new = 0
for lamdaRf in np.arange(1, 10, 0.3):
    cif, lamdaf, betaf, beta_pitchf = fun3(lamdaRf, elem, rdi, N, CD, CL, alpha)
    if "{:.3f}".format(cif[9]) == "0.065":
        lamdaRf_new = lamdaRf
        print("𝜆𝑟 of turbine estas V90-2.0 MW =", lamdaRf_new)
# Fast turbines as the Vestas 90 shown are considered.
# we notice that the blade root chord lengths obtained with the theory
# are overestimated and not suitable to obtain a smooth junction to the hub.
# The chord length near the hub is large, we want to decrease it.
# Than we modify alpha
alpha1 = np.zeros(elem)
for i in range(0, elem):
    alpha1[i] = alpha_best - 5 * (1 - (1 / np.sqrt(rdi[i])))
plt.figure()
plt.plot(rdi, alpha1)
plt.xlabel("rdi")
plt.ylabel("alphai")

# we interpolate CL and CD and recalculate previous values of the blade chord
CD_interp, CL_interp = fun2(elem, alpha1)
plt.figure()
for lamdaR in range(1, 11):
    ci_interp, lamda_interp, beta_interp, beta_pitch_interp = fun3(
        lamdaR, elem, rdi, N, CD_interp, CL_interp, alpha1
    )
    plt.plot(rdi, ci_interp, label=lamdaR)
plt.xlabel("rdi")
plt.ylabel("ci first correction")
plt.legend()
print("We notice chord discontinuity at hub, and the drag is in an sudden" " increase.")

# Second interpolation
alpha_stall = 0
for i in range(size):
    if df["CL"][i] == np.max(df["CL"]):
        alpha_stall = df["alpha"][i]
x = (alpha_best - alpha_stall) / (1 - (1 / np.sqrt(rdi[0])))
alpha2 = np.zeros(elem)
for i in range(0, elem):
    alpha2[i] = alpha_best - x * (1 - (1 / np.sqrt(rdi[i])))
CD_interp2, CL_interp2 = fun2(elem, alpha2)
plt.figure()
for lamdaR in range(1, 11):
    ci_interp2, lamda_interp2, beta_interp2, beta_pitch_interp2 = fun3(
        lamdaR, elem, rdi, N, CD_interp2, CL_interp2, alpha2
    )
    plt.plot(rdi, ci_interp2, label=lamdaR)
plt.xlabel("rdi")
plt.ylabel("ci second correction")
plt.legend()

# Plot blade chords for lamdaR =7 in the 3 cases: original design and the two
# modified ones
fig = plt.figure()
ax1 = fig.add_subplot(332, xlabel="rdi", ylabel="ci original \n for lamdaR =7")
ax2 = fig.add_subplot(334, sharex=ax1, ylabel="ci first correction\n for lamdaR =7")
ax3 = fig.add_subplot(336, sharex=ax1, ylabel="ci second correction \n for lamdaR =7")
ci_7, lamda_7, beta_7, beta_pitch_7 = fun3(7, elem, rdi, N, CD, CL, alpha)
ax1.plot(rdi, ci_7)
ci_7interp, lamda_7interp, beta_7interp, beta_pitch_7interp = fun3(
    7, elem, rdi, N, CD_interp, CL_interp, alpha1
)
ax2.plot(rdi, ci_7interp)
ci_7interp2, lamda_7interp2, beta_7interp2, beta_pitch_7interp2 = fun3(
    7, elem, rdi, N, CD_interp2, CL_interp2, alpha2
)
ax3.plot(rdi, ci_interp2)

# The last blade design is now considered, we need to calculate the percentage
# of the rotor disk occupied by the blades which is 𝑆B/SD using the last angle
# of attack correction knowing that SD is the rotor disk surface and SB is
# the projection of the blades on the rotor plan.
sbdp = np.zeros(10)
for lamdaR in range(1, 11):
    ci_interp2, lamda_interp2, beta_interp2, beta_pitch_interp2 = fun3(
        lamdaR, elem, rdi, N, CD_interp2, CL_interp2, alpha2
    )
    SBD_pourcentage = SBD(elem, ci_interp2, beta_pitch_interp2)
    sbdp[lamdaR - 1] = SBD_pourcentage
lamdaR = range(1, 11)
plt.figure()
plt.plot(lamdaR, sbdp)
plt.xscale("log")
plt.yscale("log")
plt.xlabel("lamdaR")
plt.ylabel("SBD [%]")

# Calculate CT (rotor drag coefficient), CM (rotor torque coefficient) and CP
# (rotor powercoefficient) as a function of lamdaR.
CMT = np.zeros(10)
CPT = np.zeros(10)
CTT = np.zeros(10)
for lamdaR in range(1, 11):

    ci_interp2, lamda_interp2, beta_interp2, beta_pitch_interp2 = fun3(
        lamdaR, elem, rdi, N, CD_interp2, CL_interp2, alpha2
    )
    CMb = CM(
        elem, lamda_interp2, ci_interp2, CL_interp2, CD_interp2, beta_interp2, rdi, 1
    )
    CMT[lamdaR - 1] = CMb
    CPT[lamdaR - 1] = lamdaR * CMT[lamdaR - 1]
    CTT[lamdaR - 1] = (8 / 9) * (1 - (0.05) ** 2)
lamdaR = range(1, 11)
plt.figure()
plt.plot(lamdaR, CPT)
plt.xlabel("lamdaR")
plt.ylabel("CP")

# Recalculate the CP(lamdaR) curve for different LDRmod corresponding to
# typical aerodynamics applications and 2D foil from Sandia.]
CMT2 = np.zeros(10)
CPT2 = np.zeros(10)
CTT2 = np.zeros(10)
lamdaR_M = range(1, 11)
LDR_modified = {"LDR_Paragliding": "10", "LDR_Glider": "30", "LDR_Sandia": "76"}
plt.figure()
for key, value in LDR_modified.items():
    X = LDR_best / int(value)
    for lamdaR in range(1, 11):
        ci_interp2, lamda_interp2, beta_interp2, beta_pitch_interp2 = fun3(
            lamdaR, elem, rdi, N, CD_interp2, CL_interp2, alpha2
        )
        CMb2 = CM(
            elem,
            lamda_interp2,
            ci_interp2,
            CL_interp2,
            CD_interp2,
            beta_interp2,
            rdi,
            X,
        )
        CMT2[lamdaR - 1] = CMb2
        CPT2[lamdaR - 1] = lamdaR * CMT2[lamdaR - 1]
        CTT2[lamdaR - 1] = (8 / 9) * (1 - (0.05) ** 2)
    plt.plot(lamdaR_M, CPT2, label=key)
plt.xlabel("lamdaR")
plt.ylabel("CP")
plt.legend()
for i in plt.get_fignums():
    plt.figure(i)
    plt.savefig("figure%d.png" % i)
# save figures
for i in plt.get_fignums():
    plt.figure(i)
    plt.savefig("figure%d.png" % i)
