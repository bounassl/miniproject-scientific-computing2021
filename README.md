# miniproject-scientific-computing2021

# Topic: Renewable marine energy

# Objective: 
Blade design and Power coefficient CP function of the tip speed ratio λR

# Tasks:

1- Best lift-to-drag ratio: The aim is to size a 3 blades wind turbine Vestas 90 thanks to a excel Book1 Values of aerodynamics coefficients CL and CD.

2-Evaluate the λR of american wind turbine(slow turbine), and fast turbine

3- Chord and rotor surface determination: The main objective is to plot the blade shape from R to Rhub and to illustrate the strong decrease of the total blade surface of a rotor as its design tip speed ratio λR increases. To do that, a loop from λR varying from 1 to 10 is proposed.

4-Power coefficient as a function  λR and lift to drag ratio for Vestas 90 turbibne, and recalculate for 3 aerodynamics applications : Paragliding, Glider, and airfoil (Re=2.106 – from SANDIA).

# Environment used: 
Spyder(Python3.9)

# Note: 
I installed the library openpyxl to read/write Excel 2010 xlsx/xlsm/xltx/xltm files

# Functions used:
1- fun2 in py1.py file calculate the interpolations of CL(α) and CD (α)

2- fun3 in py2.py to to create table : rdi, lamda= rdi*lamdaR,betai (flow angle in °), alphai (flow incidence in °), betapi (pitch angle in °), Cli, Cdi, ci=Ci/R (dimensionless chord), 𝑖∈[1,number of element=19].

3- SBD in py3.py file to calculate the percentage of the rotor disk occupied by the blades which is 𝑆𝐵/𝑆𝐷 using the last angle of attack correction knowing that SD is the rotor disk surface and SB is the projection of the blades on the rotor plan. 𝑆𝐵/𝑆𝐷= 𝑁𝜋Σ(𝐶i/𝑅) *𝑅∗(Δ𝑟/𝑅)*cos𝛽pi

4- CM in py4.py file to calculate the CM (rotor torque coefficient)

# Main file of the project: project.pynb

# Figures ploted:
Figure 1: CL, CD versus α.

Figure2: LDR versus α.

Figure 3: Ci/R original versus rdi.

Figure 4: alphai versus rdi.

Figure 5: Ci/R first correction versus rdi.

Figure 6: Ci/R second correction versus rdi.

Figure 7: blade chords Ci/R for λR =7 in the 3 cases: original design and the two modified.

Figure 8:SB/SD in percentage as a function of λR using a log/log scale.

Figure 9: CP (rotor powercoefficient) as a function of λR for Vestas 90 turbine.

Figure 10:CP (rotor powercoefficient) as a function of λR for 3 others  aerodynamics applications.




