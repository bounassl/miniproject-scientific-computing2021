# -*- coding: utf-8 -*-
"""
Created on Mon Jan  3 00:20:10 2022

@author: acer
"""

import numpy as np
import math


def fun2(elem, alpha):
    CD_interp = np.zeros(elem)
    CL_interp = np.zeros(elem)
    for i in range(0, elem):
        CL_interp[i] = (
            (1.8229 * 10 ** -6 * alpha[i] ** 5)
            - (7.22945 * 10 ** -5 * alpha[i] ** 4)
            + (7.48047 * 10 ** -4 * alpha[i] ** 3)
            - (3.66841 * 10 ** -3 * alpha[i] ** 2)
            + (1.17188 * 10 ** -1 * alpha[i])
            - (2.11424 * 10 ** -3)
        )
        if int(alpha[i]) in range(1, 16):
            CD_interp[i] = (
                (7.4703302 * 10 ** -5 * alpha[i] * alpha[i])
                - (1.8618421 * 10 ** -4 * alpha[i])
                + (7.8097007 * 10 ** -3)
            )
        if int(alpha[i]) in range(16, 19):
            CD_interp[i] = (0.107 * alpha[i]) - 1.688
        else:
            CD_interp[i] = (2.3903571 * 10 ** -2 * alpha[i]) - (1.9473929 * 10 ** -1)
    return CD_interp, CL_interp
